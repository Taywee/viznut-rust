use rodio::source::Source;
use rodio::{OutputStream, Sink};

use viznut::Viznut;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let (_sink, stream_handle) = OutputStream::try_default()?;
    let sink = Sink::try_new(&stream_handle)?;
    sink.append(Viznut::new().amplify(0.2));
    sink.sleep_until_end();
    Ok(())
}
