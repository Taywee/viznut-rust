use rodio::source::Source;

use std::time::Duration;

/// The list of notes, encoded as a bytes array.
const NOTES: [u8; 32] = *b"`cW`g[`cgcg[eYcb^bV^eW^be^bVecb^";

/// 48KHz
const SAMPLE_RATE: u32 = 48000;
const SAMPLE_RATE_F: f64 = SAMPLE_RATE as f64;
const SAMPLE_RATE_2: f64 = (SAMPLE_RATE as u64 * SAMPLE_RATE as u64) as f64;

/// 8 beats per second
const SAMPLES_PER_BEAT: u32 = SAMPLE_RATE / 8;

/// A Sample iterator for the viznut signature.
pub struct Viznut {
    frequency_ramp: f64,
    bass_ramp: f64,
    base_bass_frequency: f64,
    bass_frequency: f64,
    beat: u8,
    frequency: f64,
    sample: u32,
    phrase: u8,
}

impl Viznut {
    pub fn new() -> Self {
        Default::default()
    }
}

impl Default for Viznut {
    fn default() -> Self {
        let note = NOTES[0];
        let frequency = 2.0f64.powf(1.0 / 12.0).powi(note.into());
        let bass_frequency = frequency / 4.0;
        let base_bass_frequency = bass_frequency;
        Self {
            frequency_ramp: 0.0,
            bass_ramp: 0.0,
            bass_frequency,
            base_bass_frequency,
            beat: 0,
            frequency,
            sample: 0,
            phrase: 0,
        }
    }
}

impl Iterator for Viznut {
    type Item = f32;

    fn next(&mut self) -> Option<Self::Item> {
        let samples_left = (SAMPLES_PER_BEAT - self.sample) as f64;

        // The ramps go from 0 to the SAMPLE_RATE repeatedly by the frequency.
        self.bass_ramp = (self.bass_ramp + self.bass_frequency) % SAMPLE_RATE_F;
        self.frequency_ramp = (self.frequency_ramp + self.frequency) % SAMPLE_RATE_F;

        // The bass ramp is actually used to just compare to samples left,
        // acting as a distorted square wave rather than a sawtooth. The
        // samples_left * 4 makes the bass half the volume of the lead.
        // The bass is in the range [-0.5, 0.5]
        let output = samples_left * 4.0 / SAMPLE_RATE_F
            * if self.bass_ramp <= samples_left {
                1.0
            } else {
                -1.0
            }
            // frequency_ramp has a max of SAMPLE_RATE.  samples_left has a
            // max of SAMPLE_RATE / 8, therefore the numerator has a max of
            // SAMPLE_RATE ** 2.  This calculation ramps from 1.0 to 0.0, using
            // the frequency ramp and also fading to silence at the end of the
            // note.
            + self.frequency_ramp * samples_left * 8.0 / SAMPLE_RATE_2
            // Adding the bass to the lead takes us into a range of [-0.5, 1.5],
            // so this final subtraction takes it into [-1.0, 1.0]
            - 0.5;

        self.sample += 1;

        if self.sample >= SAMPLES_PER_BEAT {
            self.beat += 1;
            self.sample = 0;

            if self.beat >= NOTES.len() as u8 {
                self.beat = 0;
                self.phrase += 1;

                // Only keep track of 4 phrases at a time.
                if self.phrase >= 4 {
                    self.phrase = 0;
                }
            }

            let mut note = NOTES[self.beat as usize];

            // Every two phrases, shift up 3 semitones.
            if self.phrase > 1 {
                note += 3;
            }

            // Use "scientific pitch"
            self.frequency = 2.0f64.powf(1.0 / 12.0).powi(note.into());

            // Every 8 beats, reset the base bass frequency.
            if self.beat % 8 == 0 {
                self.base_bass_frequency = self.frequency / 4.0;
            }

            // The actual bass frequency bounces up and down an octave per note
            // from the base.
            self.bass_frequency = if self.beat & 1 == 0 {
                self.base_bass_frequency
            } else {
                self.base_bass_frequency / 2.0
            };
        }

        Some(output as f32)
    }
}

impl Source for Viznut {
    fn current_frame_len(&self) -> Option<usize> {
        None
    }

    fn channels(&self) -> u16 {
        1
    }

    fn sample_rate(&self) -> u32 {
        SAMPLE_RATE
    }

    fn total_duration(&self) -> Option<Duration> {
        None
    }
}
